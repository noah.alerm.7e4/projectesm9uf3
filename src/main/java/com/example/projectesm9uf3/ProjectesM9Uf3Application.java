package com.example.projectesm9uf3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectesM9Uf3Application {

    public static void main(String[] args) {
        SpringApplication.run(ProjectesM9Uf3Application.class, args);
    }

}
